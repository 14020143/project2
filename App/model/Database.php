<?php
/**
 * Created by PhpStorm.
 * User: hainguyen
 * Date: 4/11/2017
 * Time: 11:18 PM
 */
namespace App\model;
use App\model\inter;
require_once "Interfacedatabase.php";
$GLOBALS['config'] = include 'D:\server\www\TranLeMVC\App\model\fileconf.php';
class Database implements inter\Interfacedatabase {
    protected $severname;
    protected $dbuser;
    protected $dbpass;
    protected $dbname;
    protected $conn = NULL;
    protected $result = NULL;

    public function __construct()
    {

        $this->severname= $GLOBALS['config']['host'];
        $this->dbuser= $GLOBALS['config']['username'];
        $this->dbpass= $GLOBALS['config']['password'];
        $this->dbname= null;

    }

    /**
     * @param null $conn
     */

    public function connectDB($sever_name = '',$dbuser = '',$dbpass='',$dbname) //kết nối cơ sở dữ liệu
    {
        $this->severname=(!empty($sever_name)?$sever_name:$this->severname);
        $this->dbuser=(!empty($dbuser)?$dbuser:$this->dbuser);
        $this->dbpass=(!empty($dbpass)?$dbpass:$this->dbpass);
        $this->dbname=$dbname;
        $this->conn = new \mysqli($this->severname,$this->dbuser,$this->dbpass,$this->dbname);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } else {
            echo __METHOD__;
        }
        return $this->conn;
    }


    public function createDB($sever_name = '',$dbuser = '',$dbpass='',$dbname)// tạo cơ sở dữ liệu
    {
        $this->severname=(!empty($sever_name)?$sever_name:$this->severname);
        $this->dbuser=(!empty($dbuser)?$dbuser:$this->dbuser);
        $this->dbpass=(!empty($dbpass)?$dbpass:$this->dbpass);
        $this->dbname=$dbname;
        $this->conn= new \mysqli($this->severname,$this->dbuser,$this->dbpass);
        if ($this->conn->connect_error){
            die ("Connecting Failded ". $this->conn->connect_error);
        }
        echo "Connecting";
       echo $sql ="CREATE DATABASE $dbname ";

        if ($this->conn->query($sql)=== true){
            echo" Create database succesfully";
        }
        else {
            echo "Error  creating  database ".$this->conn->error;
        }
        $this->dbname = $dbname;
        $this->conn->close();
    }


 /*   public function closeDb()// Đóng kết nối cơ sở dữ liệu
    {
     $this->conn->close();

    }*/

    public function query($sever_name = '',$dbuser = '',$dbpass='',$dbname,$sql) // Thực hiện một số câu lênh cơ sở dữ liệu
    {
        $this->severname=(!empty($sever_name)?$sever_name:$this->severname);
        $this->dbuser=(!empty($dbuser)?$dbuser:$this->dbuser);
        $this->dbpass=(!empty($dbpass)?$dbpass:$this->dbpass);
        $this->dbname=$dbname;
        $this->connectDB($this->severname,$this->dbuser,$this->dbpass,$this->dbname);
        $this->result= $this->conn->query($sql);
        return $this->result;
    }

    public function deleteDb($sever_name = '',$dbuser = '',$dbpass='',$dbname)// Xóa cơ sở dữ liệu
    {
        $this->severname=(!empty($sever_name)?$sever_name:$this->severname);
        $this->dbuser=(!empty($dbuser)?$dbuser:$this->dbuser);
        $this->dbpass=(!empty($dbpass)?$dbpass:$this->dbpass);
        $this->dbname=$dbname;
        $this->connectDB($this->severname,$this->dbuser,$this->dbpass,$this->dbname);
        if ($this->conn->connect_error){
            die ("Connecting Failded ". $this->conn->connect_error);
        }
        echo "Connecting";
        $sql ="DROP DATABASE  $dbname";
        if ($this->conn->query($sql)=== true)
        {
            echo " Drop database succesfully";
        } else {
            echo " Drop database failed";
        }
    }
}
?>