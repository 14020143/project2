<?php
namespace App\model\inter;
/**
 * Created by PhpStorm.
 * User: hainguyen
 * Date: 4/13/2017
 * Time: 9:37 PM
 */
 interface Interfacedatabase{
       public function createDB($sever_name,$dbuser,$dbpass,$dbname);
       public function connectDB($sever_name ,$dbuser ,$dbpass,$dbname);
       public function query($sever_name ,$dbuser ,$dbpass,$dbname,$sql);
       public function deleteDb($sever_name,$dbuser,$dbpass,$dbname);
 }