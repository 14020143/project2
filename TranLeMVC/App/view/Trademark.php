<div class="trademark">
    <div class="custom1">
        <div class="slide-holder">
            <div class="slide-pager">
                <div class="slide-control-prev"><img src="../../public/assets/img/a03.1.png"/></div>
                <div class="slide-control-next"><img src="../../public/assets/img/a03.2.png"/></div>
            </div>
            <div class="slide-container">
                <div class="slide-stage">
                    <div class="slide-image"><img src="../../public/assets/img/tra1.png"/></div>
                    <div class="slide-image"><img src="../../public/assets/img/tra2.png"/></div>
                    <div class="slide-image"><img src="../../public/assets/img/tra3.png"/></div>
                    <div class="slide-image"><img src="../../public/assets/img/tra4.png"/></div>
                    <div class="slide-image"><img src="../../public/assets/img/tra5.png"/></div>
                    <div class="slide-image"><img src="../../public/assets/img/tra6.png"/></div>
                    <div class="slide-image"><img src="../../public/assets/img/tra7.png"/></div>

                </div>
            </div>
        </div>

    </div>
    <div class="custom2">
        <div class="dstab">
            <div class="tab-wrapper">
                <ul class="tab">
                    <li>
                        <a href="#tab-main-info">NEW ARRIVALS</a>
                    </li>
                    <li>
                        <a href="#tab-image">POPULAR ITEMS</a>
                    </li>
                    <li>
                        <a href="#tab-seo">SALE ITEMS</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-item" id="tab-main-info"><!--tab 1 : new arrival-->
                <div class="cover">
                    <div id="c1">NEW <br>ARRIVALS</div>
                    <div id="c2">Check out our latest products</div>
                </div>
                <div class="slide-sp">                           <!--dong bat dau slide 2-->
                    <div class="slide_nho">

                        <!--<section class="regular slider" style="width: 560px;height: 260px; margin: 0px;">
                            <div>
                                <img src="../assets/img/sp1.png">
                                <div id="MoTa">AFL Team Jersey 2016<br>West Coast Eagle</div>
                                <div id="Gia">$210.00</div>
                            </div>
                            <div>
                                <img src="../assets/img/sp2.png">
                                <div id="MoTa">AFL Team Jersey 2016<br>West Coast Eagle</div>
                                <div id="Gia">$210.00</div>
                            </div>
                            <div>
                                <img src="../assets/img/sp3.png">
                                <div id="MoTa">AFL Team Jersey 2016<br>West Coast Eagle</div>
                                <div id="Gia">$210.00</div>
                            </div>


                        </section>
-->
                    </div>

                </div>                                      <!--dong ket thuc slide 2-->
                <!-- .tab_container -->
            </div>
            <div class="tab-item" id="tab-image"><!--tab2: popular-->
                <div class="cover">
                    <div id="c1">POPULAR <br>ITEMS</div>
                    <div id="c2">Check out our latest products</div>
                </div>
                <div class="slide-sp">
                    <div class="slide_nho">
                        <!--slide nho -->

                    </div>

                </div>
            </div>
            <div class="tab-item" id="tab-seo"><!--tab3:sale item-->
                <div class="cover">
                    <div id="c1">SALE <br>ITEMS</div>
                    <div id="c2">Check out our latest products</div>
                </div>
                <div class="slide-sp">
                    <div class="slide_nho">
                        <!--slide nho -->

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>