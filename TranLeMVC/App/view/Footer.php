<div class="footer">
    <div class="btnJoin">
        <div id="textJoin">JOIN OUR ELITE CLUB TO RECEIVE LATEST UPDATES & GREAT OFFERS</div>
        <div class="boxJ">
            <input type="text" placeholder="   Enter your email address...">
            <button type="submit" id="JoinNow">
                <div>JOIN NOW</div>
            </button>
        </div>
    </div>
    <div class="info">
        <div id="IF1">
            <img src="../../public/assets/img/logo_footer.png">
            <div id="descri">Unlike most sellers, Savvy Supporter grew out of the general love of Australian Sport.
                We have both been passionate Rugby League fans since childhood, and the growth of Savvy Supporter over
                the past couple of
                years has enabled them to spend their time focusing on what they love most – <b>talking and thinking
                    about sport.</b>
            </div>
            <div id="phone">
                <img src="../../public/assets/img/ic_phone.png">
                <b>Need Help ?</b>
            </div>
            <div id="soLienHe">1300 275 728</div>
        </div>
        <div id="IF2">
            <div id="tieude"><b>INFORMATION</b></div>
            <div id="dsach">
                <ul>
                    <li>Home</li>
                    <li>About Savvy</li>
                    <li>FAQ</li>
                </ul>
            </div>
        </div>
        <div id="IF3">
            <div id="tieude"><b>PRODUCTS</b></div>
            <div id="dsach">
                <ul>
                    <li>NRL</li>
                    <li>AFL</li>
                    <li>Rugby Union</li>
                    <li>A-League</li>
                    <li>Football</li>
                    <li>Savvy Sales</li>
                </ul>
            </div>
        </div>
        <div id="IF4">
            <div id="tieude"><b>SUPORT CENTRE</b></div>
            <div id="dsach">
                <ul>
                    <li>Contact Us</li>
                    <li>Terms & Conditions</li>
                    <li>Delivery & Returns</li>
                    <li>Exchange Policy</li>
                    <li>Warranty</li>
                    <li>Privacy Policy</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="NhaSanXuat">
        <div id="textNSX">© 2016, Savvy Supporter - All rights reserved.</div>
    </div>
</div>