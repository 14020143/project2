
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Home</title>

    <link rel="stylesheet" href="../../public/assets/css/css_home.css">
    <link rel="stylesheet" href="../../public/assets/css/slide.css">
    <link rel="stylesheet" href="../../public/assets/css/slide_trademark.css">
    <link rel="stylesheet" href="../../public/assets/slick/slick.scss">
    <link rel="stylesheet" href="../../public/assets/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../../public/assets/slick/slick.css">

    <link href="https://fonts.googleapis.com/css?family=Asap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <script language="javascript" src="../../public/assets/js/tab.js"></script>
    <script language="javascript" src="../../public/assets/js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script language="javascript" src="../../public/assets/js/jquery-1.9.1.min.js"></script>
    <script language="javascript" src="../../public/assets/js/custom.js"></script>
    <script language="javascript" src="../../public/assets/js/tab.js"></script>





    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="../../public/assets/slick/slick.js" type="text/javascript" charset="utf-8"></script>


</head>
<body class="wall">
<div class="over">
    <div class="left"></div>
    <div class="all">
        <?php
        include "Header.php";
        include "Search.php";
        include "Menu.php";
        include "Banner.php";
        include "Four.php";
        include "Trademark.php";
        ?>
    </div>

</div>
<div class="right"></div>
        <?php
        include "Footer.php";
        ?>
</body>

</html>