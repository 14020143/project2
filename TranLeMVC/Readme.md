# Write by Tran Le and Hai Nguyen  #


### Introduce ###

* Quick summary : exercise 2 in company is make a Framework by PHP.
* Version : Finally


### Set up  ###

* Use JetBrains PhpStorm for coding
* Use WPN-XM ServerControlPanel - is an open source web server solution stack for professional PHP development on the Windows platform.
###  How to use FrameWork  ###
The first pull the Framework from git . Move file cloned to folder DOCUMENT_ROOT web server ex: in nginx is ``` C:\server\www ```.
Devide URL into 2 parts and separated by ```/```. On the right ```/``` is the modules you want use for your project . in this Framework have 2 modules is Product and User . ex : ```tranlemvc.dev/Product``` => modules is Product. 
In each section , include 3 file  . 

![Untitled.pngasd.png](https://bitbucket.org/repo/MrgoAzj/images/2485922197-Untitled.pngasd.png)

In controllerProduct.php , you can use function about Product for your purpose . The same for another controller modules.
In View.php , you can include your view from  Folder View . 
In Model.php , you can use to make object from Database.php in folder model . 
### Autoload ###
This Framework have support Autoload and use composer . 
### Directory structure  ###
![Untitled.png](https://bitbucket.org/repo/MrgoAzj/images/627012040-Untitled.png)

* App : include  view,controller and model main of FrameWork and include modules.
* Public : is assets include forlder ```css,font,img,js,.... ```for Framework 
* Vendor : include file of composer for framework .
* index.php : the first file run in Framework .