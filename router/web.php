<?php


namespace router;

class web
{
    public static function run()
    {
        self::home();

       // self::autoload();

        self::dispatch();
    }

    private static function home()
    {
        $arrURI = explode('/', $_SERVER['REQUEST_URI']); // tách chuỗi thành các chuỗi nhỏ

        define("_CONTROLLER",
            (isset($arrURI[1]) && $arrURI[1] != null) ? ($arrURI[1]) : 'index');


        define("_ACTION",
            (isset($arrURI[2]) && $arrURI[2] != null) ? ($arrURI[2]) : 'index');


    }

   /* private static function autoload()
    {
        spl_autoload_register(function ($classname) {

            if (file_exists(str_replace('\\', '/', $classname) . '.php')) {

                require_once str_replace('\\', '/', $classname) . '.php';
            } else {
                echo "Loi";
                exit();
            }
        }
        );
    }*/
   // Đã thêm vào composer

    // sau đó chạy cái này
    private static function dispatch()//bắt controller vào
    {
        $controller_classname = '\\App\\controller\\' . _CONTROLLER . "Controller";

        $action_name = _ACTION;

        $controller = new $controller_classname;

        if (method_exists($controller_classname, $action_name)) {
                $controller->$action_name();
        } else {

            echo "Can't find the method in controller class";// Nếu không bắt được sự kiện thì trả về câu trả lời
            exit();
        }

    }

}
